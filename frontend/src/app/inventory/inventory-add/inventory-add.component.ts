import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InventoryRequest } from '../inventory.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-add',
  templateUrl: './inventory-add.component.html',
  styleUrls: ['./inventory-add.component.scss']
})
export class InventoryAddComponent implements OnInit {

  inventoryForm = new FormGroup({
    name: new FormControl(null, [Validators.maxLength(100)]),
    location: new FormControl(null, [Validators.maxLength(50)])
  })

  constructor(private router: Router, private inventoryService: InventoryService) { }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if(this.inventoryForm.invalid){
      console.log('This form is invalid!');
      console.log(this.inventoryForm.errors);
      return;
    }
    this.inventoryService.create(new InventoryRequest(this.inventoryForm.value.name,  this.inventoryForm.value.location)).subscribe({
      next: _ => this.router.navigateByUrl('inventories')
    });

  }

}
