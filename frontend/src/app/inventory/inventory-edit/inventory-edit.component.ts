import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryRequest } from '../inventory.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-edit',
  templateUrl: './inventory-edit.component.html',
  styleUrls: ['./inventory-edit.component.scss']
})
export class InventoryEditComponent implements OnInit {

  inventoryForm = new FormGroup({
    name: new FormControl(null, [
      Validators.maxLength(100)
    ]),
    location: new FormControl(null, [
      Validators.maxLength(50)
    ])
  })

  private id: number | undefined;

  constructor(private route: ActivatedRoute, private inventoryService: InventoryService, private router: Router) { }

  ngOnInit(): void {
    const rawId = this.route.snapshot.paramMap.get('id');
    if(rawId){
        this.id = Number(rawId);
        this.inventoryService.getInventoryById(this.id).subscribe({
          next: inventory => {
            this.inventoryForm.get('name')?.setValue(inventory.name);
            this.inventoryForm.get('location')?.setValue(inventory.location);
          }
        })
    }
  }

  public onSubmit(): void {
    if(this.inventoryForm.invalid){
      console.log('This form is invalid!');
      console.log(this.inventoryForm.errors);
      return;
    }
    const name = this.inventoryForm.value.name;
    const location = this.inventoryForm.value.location;
    if(this.id){
    this.inventoryService.update(this.id, new InventoryRequest(name, location)).subscribe({
      next: _ => this.router.navigateByUrl('inventories')
    })
    }
  }

}
