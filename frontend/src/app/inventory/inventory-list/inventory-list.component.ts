import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmationPopupComponent } from 'src/app/shared/confirmation-popup/confirmation-popup.component';
import { Page } from 'src/app/shared/shared.domain';
import { Inventory } from '../inventory.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {

  public page: Page<Inventory> = new Page<Inventory>([], 0, 0);
  public bsModalRef: BsModalRef |undefined;
  public saveName = 'save';

  constructor(private inventoryService: InventoryService, private router: Router, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getPage(0, 10);
  }

  public onClick():void {
    this.router.navigateByUrl('/inventories/add');
  }
  
  public onEdit(inventoryId:number): void {
    this.router.navigateByUrl(`/inventories/${inventoryId}`);
  }

  public onDelete(inventoryId:number): void {
   

    const initialState = {
      modalText: `Are you sure you want to delete the inventory with id: ${inventoryId}`
    }

    this.bsModalRef = this.modalService.show(ConfirmationPopupComponent, {initialState});
    this.bsModalRef.onHide?.subscribe(_ =>{
      if(this.bsModalRef?.content.confirmed){
        this.inventoryService.delete(inventoryId).subscribe({
          next: _ => this.getPage(0,10)
        }
        );
      }
      this.getPage(0,10);
    });

  }

  public getIconName():string {
    return 'save';
    }

  public onTestClick(message:string):void {
    console.log(message);
  }

  public onRowClicked(inventoryId:number):void {
    this.router.navigateByUrl(`inventories/${inventoryId}/products`)
  }

  private getPage(page: number, size:number): void {
    this.inventoryService.getPage(page, size).subscribe({
      next: response => this.page = response
    })
  }

}
