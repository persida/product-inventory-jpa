import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryAddComponent } from './inventory-add/inventory-add.component';
import { InventoryEditComponent } from './inventory-edit/inventory-edit.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryComponent } from './inventory.component';
import { AllProductsComponent } from './products/all-products/all-products.component';

const routes: Routes = [
  {path:'', component: InventoryComponent, children: [
    {path: '', component: InventoryListComponent},
    {path: 'products',  component: AllProductsComponent},
    {path: 'add', component: InventoryAddComponent},
    {path: ':id', component: InventoryEditComponent},
    {path: ':id/products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
