import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { SharedModule } from '../shared/shared.module';
import { InventoryComponent } from './inventory.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryService } from './inventory.service';
import { InventoryAddComponent } from './inventory-add/inventory-add.component';
import { InventoryEditComponent } from './inventory-edit/inventory-edit.component';



@NgModule({
  declarations: [
    InventoryListComponent,
    InventoryComponent,
    InventoryAddComponent,
    InventoryEditComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InventoryRoutingModule
  ],
  providers: [
    InventoryService
  ]
})
export class InventoryModule { }
