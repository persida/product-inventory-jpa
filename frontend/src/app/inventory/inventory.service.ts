import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Inventory, InventoryRequest } from './inventory.domain';
import { environment } from 'src/environments/environment';
import { Page } from '../shared/shared.domain';
import { catchError, map, tap } from 'rxjs/operators';
import { AlertService } from '../shared/components/alert/alert.service';

const API_ENDPOINT = 'inventories';
const HEADERS = new HttpHeaders({
  Accept: 'application/json'
});

@Injectable()
export class InventoryService {

  constructor(private httpClient: HttpClient, private alertService: AlertService) { }

  public getPage(page: number, size: number): Observable<Page<Inventory>> {
      const params = new HttpParams()
      .append('page', page)
      .append('size', size);
      return this.httpClient.get<Page<Inventory>>(`${environment.apiUrl}/${API_ENDPOINT}`, {headers: HEADERS, params: params}).pipe(
        tap(_ => this.alertService.successAlert('Page retrieved!')),
        catchError((error) => {
          this.alertService.errorAlert(error);
          return of(new Page<Inventory>([], 0, 0));
        })
      );
      
  }

  public create(request: InventoryRequest): Observable<Inventory> {
    return this.httpClient.post<Inventory>(`${environment.apiUrl}/${API_ENDPOINT}`, request, {headers: HEADERS}).pipe(
      catchError(error => {
        this.alertService.errorAlert(error);
        return throwError(error);
      })
    )
  }

  public getInventoryById(id:number): Observable<Inventory>{
    return this.httpClient.get<Inventory>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, {headers: HEADERS}).pipe(
      catchError(error => {
        this.alertService.errorAlert(error);
        return throwError(error);
      })
    )
  }

  public update(id:number, request: InventoryRequest): Observable<Inventory>{
    return this.httpClient.put<Inventory>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, request, {headers: HEADERS}).pipe(
      catchError(error => {
        this.alertService.errorAlert(error);
        return throwError(error);
      })
    )
  }

  public delete(id:number): Observable<any>{
    return this.httpClient.delete<any>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, {headers: HEADERS}).pipe(
      catchError(error => {
        this.alertService.errorAlert(error);
        return throwError(error);
      })
    )
  }

}
