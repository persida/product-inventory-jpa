import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Page } from 'src/app/shared/shared.domain';
import { ProductService } from '../product.service';
import { Product } from '../products.domain';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  
  public inventoryId: number|undefined;
  public page: Page<Product> = new Page<Product>([], 0, 0);

  constructor(private productService:ProductService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const rawInventoryId = this.route.snapshot.paramMap.get('id');
    if(rawInventoryId){
      this.inventoryId = Number(rawInventoryId);
      this.productService.getPage(0, 10, this.inventoryId).subscribe({
        next: response => this.page = response
      })
    }  
  }

  onAdd(): void {
    this.router.navigate(['./add'], { relativeTo: this.route });
  }

  public onEdit(productId:number): void {
    this.router.navigate([`./${productId}`], { relativeTo: this.route });
  }

  public onDelete(productId:number): void {
    

  }

}
