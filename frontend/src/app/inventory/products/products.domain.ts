export class Product{
    constructor(
        public id: number,
        public name: string,
        public type:string,
        public price:number,
        public description: string,
        public inventoryId: number
    ){}
}

export class ProductRequest{
    constructor(
        public name: string,
        public type:string,
        public price:number,
        public description: string,
        public inventoryId: number
    ){}
}