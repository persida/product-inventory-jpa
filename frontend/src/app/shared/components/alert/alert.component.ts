import { Component, Input, OnInit } from '@angular/core';
import { Alert } from './alert.domain';
import { AlertService } from './alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  
  public timeout = 3000;

  // @Input()
  public alerts: Alert[] = [];
  

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.alertService.alertObservable.subscribe({
      next: alert => this.alerts.push(alert)
    })
  }

  // public add(type:string, message:string): void {
  //   this.alerts.push( new Alert(type, message));
  // }

  public onClosed(dismissedAlert: Alert): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
