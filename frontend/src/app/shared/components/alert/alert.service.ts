import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Alert } from './alert.domain';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private dataSource: BehaviorSubject<Alert> = new BehaviorSubject(new Alert('', ''));
  public alertObservable = this.dataSource.asObservable();

  constructor() { }

  public successAlert(message:string):void{
    const alert = new Alert('success', message);
    this.dataSource.next(alert);
  }

  public errorAlert(errorResponse:any):void{
    console.error(errorResponse);
    let message = 'An error has occurred! Please contact your administrator!';
    if(errorResponse){
      if(errorResponse.error){
        if(errorResponse.error.message){
            message = errorResponse.error.message;
        }
        else if(errorResponse.error.error_description){
          message = errorResponse.error.error_description;
        }
      }
    }
    const alert = new Alert('danger', message);
    this.dataSource.next(alert);
  }

}
