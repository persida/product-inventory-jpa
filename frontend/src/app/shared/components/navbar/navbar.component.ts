import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../../services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  constructor(private authorizationService:AuthorizationService) { }

  ngOnInit(): void {
  }

  public logout():void {
    this.authorizationService.logOut();
  }

}
