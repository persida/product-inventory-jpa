import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { PublicLayoutComponent } from './components/public-layout/public-layout.component';
import { AuthenticatedLayoutComponent } from './components/authenticated-layout/authenticated-layout.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ConfirmationPopupComponent } from './confirmation-popup/confirmation-popup.component';
import { InterceptorInterceptor } from './interceptor/interceptor.interceptor';
import { ActionButtonComponent } from './action-button/action-button.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { AlertComponent } from './components/alert/alert.component';

const COMPONENTS = [
  FooterComponent,
  PublicLayoutComponent,
  AuthenticatedLayoutComponent,
  NavbarComponent,
  ConfirmationPopupComponent,
  ActionButtonComponent,
  AlertComponent
];

const MODULES = [
  CommonModule,
  RouterModule,
  HttpClientModule,
  ReactiveFormsModule,
  MatIconModule,
  AlertModule
]

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    ...MODULES,
    ModalModule.forChild()
  ],
  exports: [
    ...COMPONENTS,
    ...MODULES
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: InterceptorInterceptor, multi: true }]
})
export class SharedModule { }
