package com.persidacvetkoska.productinventoryjpa.exception;

public class ResourceValidationException extends RuntimeException{
    public ResourceValidationException(String msg){
        super(msg);
    }
}
