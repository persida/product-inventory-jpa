package com.persidacvetkoska.productinventoryjpa.inventory;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class InventoryDto {
    public final Integer id;
    public final String name;
    public final String location;


    public InventoryDto(Integer id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;

    }
}
