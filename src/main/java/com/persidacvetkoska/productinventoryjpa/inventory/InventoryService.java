package com.persidacvetkoska.productinventoryjpa.inventory;

import com.persidacvetkoska.productinventoryjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.productinventoryjpa.inventory.product.Product;
import com.persidacvetkoska.productinventoryjpa.inventory.product.ProductDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InventoryService {
    private InventoryRepository repository;

    public InventoryService(InventoryRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public InventoryDto create(final InventoryRequest request){
        var inventory = new Inventory(request.name, request.location);
        var createdInventory = repository.save(inventory);
        return toInventoryDto(createdInventory);

    }

    public InventoryDto findByNameNative(final String name){
        var foundInventory = repository.findFirstByNameNative(name).orElseThrow(() -> new ResourceNotFoundException("Inventory not found with "+ name));
        return toInventoryDto(foundInventory);

    }

    public List<InventoryDto> findAllInventories(){

        var foundInventories = repository.findAll();
        if (foundInventories.isEmpty()){
            throw new ResourceNotFoundException("No inventories found");
        }
        return foundInventories.stream()
                .map(inventory -> toInventoryDto(inventory))
                .collect(Collectors.toList());
    }

    public Page<InventoryDto> findPage(final InventorySearchRequest request) {

        return repository.findAll(request.generateSpecification(), request.pageable)
                .map(inventory -> toInventoryDto(inventory));
    }

    public List<ProductDto> findAllProductsByType(final String type, final int inventoryId){
        var foundProducts = repository.findAllProductsByType(type, inventoryId);

        if (foundProducts.isEmpty()){
            throw new ResourceNotFoundException("No products found!");
        }
        return foundProducts.stream()
                .map(product -> toProductDto(product))
                .collect(Collectors.toList());
    }

    public InventoryDto findInventoryById(Integer id){
        var inventory = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory not found!"));
        return toInventoryDto(inventory);
    }

    public ProductDto findProductById(final Integer productId, final Integer inventoryId){

        var foundProduct = repository.findProductById(productId, inventoryId).orElseThrow(() -> new ResourceNotFoundException("Product not found!"));
        return toProductDto(foundProduct);
    }

    public List<ProductDto> findProductsByName(final String productName, final Integer inventoryId){
        List<Product> foundProducts = repository.findProductsByName(productName, inventoryId);
        if (foundProducts.isEmpty()){
            throw new ResourceNotFoundException("No products found!");
        }
        return foundProducts.stream()
                .map(product -> toProductDto(product))
                .collect(Collectors.toList());
    }

    public List<ProductDto> returnAllProducts(final Integer inventoryId){
        List<Product> products = repository.returnAllProducts(inventoryId);
        if (products.isEmpty()){
            throw new ResourceNotFoundException("No products found in inventory!");
        }
        return products.stream()
                .map(product -> toProductDto(product))
                .collect(Collectors.toList());
    }

    public InventoryDto update(Integer id, InventoryRequest inventory){
        var foundInventory = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory not found!"));
        foundInventory.name = inventory.name;
        foundInventory.location = inventory.location;
        var savedInventory = repository.save(foundInventory);
        return toInventoryDto(savedInventory);
    }

    public void delete(Integer id){
        var foundInventory = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory not found!"));
        repository.delete(foundInventory);
    }

    protected InventoryDto toInventoryDto(Inventory inventory){
        return new InventoryDto(inventory.id,inventory.name, inventory.location);
    }

    protected ProductDto toProductDto(Product product){
        return new ProductDto(product.id, product.name, product.type, product.price, product.description, product.inventory.getId());
    }
}
