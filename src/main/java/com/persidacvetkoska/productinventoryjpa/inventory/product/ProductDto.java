package com.persidacvetkoska.productinventoryjpa.inventory.product;

import java.math.BigDecimal;
import java.util.Objects;

public class ProductDto {
    public Integer id;
    public String name;
    public String type;
    public BigDecimal price;
    public String description;
    public Integer inventoryId;

    public ProductDto(Integer id, String name, String type, BigDecimal price, String description, Integer inventoryId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
        this.inventoryId = inventoryId;
    }

    public ProductDto(String name, String type, BigDecimal price, String description, Integer inventoryId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
        this.inventoryId = inventoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(type, that.type) && Objects.equals(price, that.price) && Objects.equals(description, that.description) && Objects.equals(inventoryId, that.inventoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, price, description, inventoryId);
    }
}
