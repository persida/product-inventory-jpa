package com.persidacvetkoska.productinventoryjpa.inventory.product;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class ProductRequest {
    Integer id;
    @Size(max = 50, message = "Name must be up to 50 characters")
    public String name;
    @Size(max = 50, message = "Name must be up to 50 characters")
    @NotBlank(message="Type is required")
    public String type;
    @NotNull(message = "Price is required")
    public BigDecimal price;
    @Size(max = 1000, message = "Description must be up to 1000 characters")
    public String description;
    public Integer inventoryId;

    public ProductRequest(String name,  String type, BigDecimal price,  String description, Integer inventoryId) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
        this.inventoryId = inventoryId;
    }

    public ProductRequest() {
    }
}
