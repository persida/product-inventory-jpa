package com.persidacvetkoska.productinventoryjpa.inventory.product;

import com.persidacvetkoska.productinventoryjpa.utils.VoidResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/products")
public class ProductResource {

    private ProductService productService;

    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<ProductDto> getPage(@RequestParam final Integer inventoryId, Pageable pageable){
        return productService.getPage(inventoryId, pageable);
    }

    @GetMapping(path="all", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<ProductDto> findAllPage(Pageable pageable){
        return productService.findAllPage(pageable);
    }

    @GetMapping(path = "/{productId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDto findProductById(@PathVariable final Integer productId){
        return productService.findProductById(productId);
    }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDto create(@RequestBody final ProductCreateRequest request){
        return productService.create(request);
    }

    @DeleteMapping(path="/delete", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<VoidResponse> delete(@RequestParam final Integer productId, @RequestParam final Integer inventoryId){
        productService.delete(productId, inventoryId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidResponse(true));
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDto update(@RequestBody final ProductRequest request){
        return productService.update(request);
    }
}
