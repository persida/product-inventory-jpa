package com.persidacvetkoska.productinventoryjpa.systemuser;

import com.persidacvetkoska.productinventoryjpa.systemuser.dto.SystemUserDTO;
import com.persidacvetkoska.productinventoryjpa.systemuser.dto.SystemUserRegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class SystemUserResource {

    private final SystemUserService systemUserService;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public SystemUserDTO register(@RequestBody SystemUserRegisterRequest request){
        return  systemUserService.register(request);
    }

    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<SystemUserDTO> fetchAll(
            @PageableDefault(sort = "createdAt", direction = Sort.Direction.DESC) final Pageable pageable) {
        return systemUserService.fetchPage(pageable);
    }

    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SystemUserDTO fetchByUsername(@PathVariable("username") String username) {
        return systemUserService.fetchByUsername(username);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_PLAYER'})")
//    @PutMapping(path = "/me", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public SystemUserDTO editMe(
//            @RequestBody final EditPlayerDetailsRequest request, final Principal principal) {
//        return systemUserService.edit(request, principal.getName());
//    }

    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_CLIENT', 'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public SystemUserDTO fetchMe(final Principal principal) {
        return systemUserService.fetchByUsername(principal.getName());
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
//    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public SystemUserDTO addPlayer(@RequestBody final SystemUserRegisterRequest request) {
//        return systemUserService.register(request);
//    }

//    @ApiOperation(value = "Edit profile for a users")
//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
//    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public UserView editPlayer(
//            @PathVariable("id") final String id, @RequestBody final EditPlayerDetailsRequest request) {
//        return systemUserService.edit(request, id);
//    }

//    @ApiOperation(value = "Register a new users")
//    @PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public UserView register(@RequestBody final RegisterPlayerRequest request) {
//        return systemUserService.registerPlayer(request);
//    }
//
//    @ApiOperation(value = "Enable a users")
//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
//    @PutMapping(path = "/{id}/enable", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse enablePlayer(@PathVariable("id") final String id) {
//        systemUserService.enablePlayer(id);
//        return new VoidResponse(true);
//    }

//    @ApiOperation(value = "Disable a users")
//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
//    @PutMapping(path = "/{id}/disable", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse disablePlayer(@PathVariable("id") final String id) {
//        systemUserService.disablePlayer(id);
//        return new VoidResponse(true);
//    }
//
//    @ApiOperation(value = "Delete a users")
//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
//    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse deletePlayer(@PathVariable("id") final String id) {
//        systemUserService.deletePlayer(id);
//        return new VoidResponse(true);
//    }

//    @ApiOperation(value = "Refer a new users")
//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_PLAYER'})")
//    @PostMapping(path = "/refer", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse referANewPlayer(@RequestBody final ReferNewPlayerRequest request, final Principal principal) {
//        systemUserService.referNewPlayer(request, principal.getName());
//        return new VoidResponse(true);
//    }
//
//    @ApiOperation(value = "Activate a registered users")
//    @PutMapping(path = "/activate", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse activateRegistration(@RequestBody final ActivateRegistrationRequest request) {
//        systemUserService.activateRegistration(request);
//        return new VoidResponse(true);
//    }
//
//    @ApiOperation(value = "Begin password reset flow for a users")
//    @PutMapping(path = "/password/reset", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse passwordReset(@RequestBody final PasswordResetRequest request) {
//        systemUserService.requestPasswordReset(request);
//        return new VoidResponse(true);
//    }
//
//    @ApiOperation(value = "Complete password reset flow for a users")
//    @PutMapping(path = "/password/reset/complete", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse completePasswordReset(@RequestBody final CompletePasswordResetRequest request) {
//        systemUserService.completePasswordReset(request);
//        return new VoidResponse(true);
//    }
//
//    @ApiOperation(value = "Change password for a users")
//    @PreAuthorize("hasAnyAuthority('AUTHORIZED_PLAYER')")
//    @PutMapping(path = "/password/change", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public VoidResponse passwordChange(@RequestBody final ChangePasswordRequest request, final Principal principal) {
//        systemUserService.changePassword(request, principal.getName());
//        return new VoidResponse(true);
//    }
//
//    @ApiOperation(value = "Fetch data for the platforms chart")
//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
//    @GetMapping(path = "/platforms-data", produces = MediaType.APPLICATION_JSON_VALUE)
//    public PlatformsDataResponse fetchPlatformsData(@RequestParam("year") int year) {
//        return systemUserService.fetchPlatformsData(year);
//    }
}

