package com.persidacvetkoska.productinventoryjpa.systemuser.dto;

import lombok.AllArgsConstructor;

import java.time.Instant;

@AllArgsConstructor
public class SystemUserDTO {

    public String fullName;
    public String username;
    public String email;
    public String address;


}
