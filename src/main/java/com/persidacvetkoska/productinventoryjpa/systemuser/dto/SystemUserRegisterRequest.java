package com.persidacvetkoska.productinventoryjpa.systemuser.dto;

import com.persidacvetkoska.productinventoryjpa.systemuser.model.SystemUser;
import lombok.AllArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.Instant;

@AllArgsConstructor
public class SystemUserRegisterRequest {

    @NotBlank(message = "First Name is required")
    public String fullName;
    @NotBlank(message = "Username is required")
    public String username;
    @NotBlank(message = "Password is required")
    public String password;
    @NotBlank(message = "Email is required")
    @Email(message = "Please insert valid email format")
    public String email;
    public String address;

    public SystemUser toEntity(){
        return new SystemUser(username, fullName, email, address, Instant.now(), true);
    }
}
