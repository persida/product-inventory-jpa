package com.persidacvetkoska.productinventoryjpa.systemuser.model;


import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "groups")
@ToString
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
    @Column(name = "group_name")
    public String groupName;
    @Column(name = "created_at")
    public Instant createdAt;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<GroupAuthority> groupAuthorities;

    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
    public Set<SystemUser> users = new HashSet<>();

    public Group() {
    }

    public GroupAuthority addGroupAuthority(final GroupAuthority groupAuthority) {
        groupAuthorities.add(groupAuthority);
        groupAuthority.group = this;
        return groupAuthority;
    }

    public GroupAuthority removeGroupAuthority(final GroupAuthority groupAuthority) {
        groupAuthorities.remove(groupAuthority);
        groupAuthority.group = null;
        return groupAuthority;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(id, group.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
