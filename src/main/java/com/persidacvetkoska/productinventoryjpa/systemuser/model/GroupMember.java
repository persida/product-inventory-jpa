package com.persidacvetkoska.productinventoryjpa.systemuser.model;

import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "group_members")
@ToString
public class GroupMember {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username")
    public SystemUser systemUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    public Group group;

    public GroupMember() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupMember member = (GroupMember) o;
        return Objects.equals(id, member.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
