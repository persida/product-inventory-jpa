package com.persidacvetkoska.productinventoryjpa.utils;

public class VoidResponse {

    public boolean success;

    public VoidResponse(boolean success) {
        this.success = success;
    }
}
