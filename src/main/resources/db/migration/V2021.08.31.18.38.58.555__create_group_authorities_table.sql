CREATE TABLE public.group_authorities
(
    group_id integer NOT NULL ,
    authority character varying(50)  NOT NULL,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    id integer NOT NULL,
    CONSTRAINT group_authorities_pkey PRIMARY KEY (id),
    CONSTRAINT fk_group_authorities_group FOREIGN KEY (group_id)
        REFERENCES public.groups (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)