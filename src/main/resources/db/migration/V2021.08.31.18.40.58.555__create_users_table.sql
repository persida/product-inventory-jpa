CREATE TABLE public.users
(
    username character varying(255)  NOT NULL,
    password character varying(255)  NOT NULL,
    email character varying(255)  NOT NULL,
    role character varying(255)  NOT NULL,
    enabled boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    full_name character varying(255)  NOT NULL,
    activation_key character varying(50) ,
    activation_key_created_at timestamp with time zone,
    reset_key character varying(50) ,
    reset_key_created_at timestamp with time zone,
    address character varying(255) ,
    version bigint,
    CONSTRAINT users_pkey PRIMARY KEY (username),
    CONSTRAINT users_email_key UNIQUE (email)
)