CREATE TABLE public.oauth_access_token
(
    token_id character varying(256) ,
    token bytea,
    authentication_id character varying(256)  NOT NULL,
    user_name character varying(256) ,
    client_id character varying(256) ,
    authentication bytea,
    refresh_token character varying(256) ,
    CONSTRAINT oauth_access_token_pkey PRIMARY KEY (authentication_id)
)