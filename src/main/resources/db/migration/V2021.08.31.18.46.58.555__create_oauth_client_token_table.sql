CREATE TABLE public.oauth_client_token
(
    token_id character varying(256) ,
    token bytea,
    authentication_id character varying(256) NOT NULL,
    user_name character varying(256) ,
    client_id character varying(256) ,
    CONSTRAINT oauth_client_token_pkey PRIMARY KEY (authentication_id)
)